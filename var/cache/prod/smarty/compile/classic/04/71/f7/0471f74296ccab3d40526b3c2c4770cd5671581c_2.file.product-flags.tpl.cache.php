<?php
/* Smarty version 3.1.34-dev-7, created on 2021-02-03 19:21:58
  from 'C:\wamp64\www\cicorella\themes\classic\templates\catalog\_partials\product-flags.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_601ae9c6b76c33_61941313',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0471f74296ccab3d40526b3c2c4770cd5671581c' => 
    array (
      0 => 'C:\\wamp64\\www\\cicorella\\themes\\classic\\templates\\catalog\\_partials\\product-flags.tpl',
      1 => 1612375779,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_601ae9c6b76c33_61941313 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->compiled->nocache_hash = '1534017346601ae9c6b6c6f9_41612836';
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_886685885601ae9c6b6e0b7_30046862', 'product_flags');
?>

<?php }
/* {block 'product_flags'} */
class Block_886685885601ae9c6b6e0b7_30046862 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_flags' => 
  array (
    0 => 'Block_886685885601ae9c6b6e0b7_30046862',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <ul class="product-flags">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product']->value['flags'], 'flag');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['flag']->value) {
?>
            <li class="product-flag <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['type'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['label'], ENT_QUOTES, 'UTF-8');?>
</li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </ul>
<?php
}
}
/* {/block 'product_flags'} */
}
