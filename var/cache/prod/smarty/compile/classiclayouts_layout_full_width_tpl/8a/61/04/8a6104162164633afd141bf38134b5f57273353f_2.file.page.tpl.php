<?php
/* Smarty version 3.1.34-dev-7, created on 2021-02-03 19:21:58
  from 'C:\wamp64\www\cicorella\themes\classic\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_601ae9c6e6bbd8_16757821',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8a6104162164633afd141bf38134b5f57273353f' => 
    array (
      0 => 'C:\\wamp64\\www\\cicorella\\themes\\classic\\templates\\page.tpl',
      1 => 1612375779,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_601ae9c6e6bbd8_16757821 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1668123627601ae9c6e5a832_64285454', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_1337945098601ae9c6e5d279_89578200 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_1019806236601ae9c6e5bb46_93801946 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1337945098601ae9c6e5d279_89578200', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_506260736601ae9c6e638c0_64899973 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_329946189601ae9c6e65310_34255499 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_2023087046601ae9c6e625e4_26239565 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_506260736601ae9c6e638c0_64899973', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_329946189601ae9c6e65310_34255499', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_376055220601ae9c6e68e18_74456494 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_1340157894601ae9c6e67bd2_31844344 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_376055220601ae9c6e68e18_74456494', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_1668123627601ae9c6e5a832_64285454 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_1668123627601ae9c6e5a832_64285454',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_1019806236601ae9c6e5bb46_93801946',
  ),
  'page_title' => 
  array (
    0 => 'Block_1337945098601ae9c6e5d279_89578200',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_2023087046601ae9c6e625e4_26239565',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_506260736601ae9c6e638c0_64899973',
  ),
  'page_content' => 
  array (
    0 => 'Block_329946189601ae9c6e65310_34255499',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_1340157894601ae9c6e67bd2_31844344',
  ),
  'page_footer' => 
  array (
    0 => 'Block_376055220601ae9c6e68e18_74456494',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1019806236601ae9c6e5bb46_93801946', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2023087046601ae9c6e625e4_26239565', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1340157894601ae9c6e67bd2_31844344', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
