<?php
/* Smarty version 3.1.34-dev-7, created on 2021-02-26 01:15:48
  from 'C:\wamp64\www\cicorella\themes\cicorella\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_60383db4abe192_13710714',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e19b42001bd9e1f9bbb6b047160cd28e97ac688e' => 
    array (
      0 => 'C:\\wamp64\\www\\cicorella\\themes\\cicorella\\templates\\page.tpl',
      1 => 1613597029,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60383db4abe192_13710714 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_110074188960383db4ab32a4_27298580', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_content_top'} */
class Block_155556691760383db4ab5ee0_39744028 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_95614560383db4ab7995_63325413 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_181986160960383db4ab4c70_60764221 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_155556691760383db4ab5ee0_39744028', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_95614560383db4ab7995_63325413', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_181247786760383db4abb4d0_74653008 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_68607410160383db4aba2a6_26994865 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_181247786760383db4abb4d0_74653008', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_110074188960383db4ab32a4_27298580 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_110074188960383db4ab32a4_27298580',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_181986160960383db4ab4c70_60764221',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_155556691760383db4ab5ee0_39744028',
  ),
  'page_content' => 
  array (
    0 => 'Block_95614560383db4ab7995_63325413',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_68607410160383db4aba2a6_26994865',
  ),
  'page_footer' => 
  array (
    0 => 'Block_181247786760383db4abb4d0_74653008',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_181986160960383db4ab4c70_60764221', 'page_content_container', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_68607410160383db4aba2a6_26994865', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
