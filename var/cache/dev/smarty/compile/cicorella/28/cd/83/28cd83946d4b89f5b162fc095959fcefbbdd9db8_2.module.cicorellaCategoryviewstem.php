<?php
/* Smarty version 3.1.34-dev-7, created on 2021-02-26 01:15:48
  from 'module:cicorellaCategoryviewstem' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_60383db47dac98_54847137',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '28cd83946d4b89f5b162fc095959fcefbbdd9db8' => 
    array (
      0 => 'module:cicorellaCategoryviewstem',
      1 => 1613523969,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60383db47dac98_54847137 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin C:\wamp64\www\cicorella/modules/cicorellaCategory/views/templates/hook/cicorellaCategory.tpl -->


<div class="container">

    <div class="tab-content" id="myTabContent">


            <div class="listOfCategories">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value[2], 'category', false, NULL, 'categoriesSlider', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
?>




                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['infos']['id_category'], ENT_QUOTES, 'UTF-8');?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['infos']['link_rewrite'], ENT_QUOTES, 'UTF-8');?>
">
                    <div class="categoryItem" style="background-image:url(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['category']->value['infos']['link_rewrite'],$_smarty_tpl->tpl_vars['category']->value['infos']['id_category'],''), ENT_QUOTES, 'UTF-8');?>
);">
                         <label for=""><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['infos']['name'], ENT_QUOTES, 'UTF-8');?>
</label>
                        <div class="overlayblack"></div>
                    </div>
                </a>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>







    </div>

</div>
<!-- end C:\wamp64\www\cicorella/modules/cicorellaCategory/views/templates/hook/cicorellaCategory.tpl --><?php }
}
