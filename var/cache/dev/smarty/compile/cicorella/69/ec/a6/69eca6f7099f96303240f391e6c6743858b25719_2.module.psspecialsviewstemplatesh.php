<?php
/* Smarty version 3.1.34-dev-7, created on 2021-02-26 01:15:48
  from 'module:psspecialsviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_60383db49a7999_50296516',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '69eca6f7099f96303240f391e6c6743858b25719' => 
    array (
      0 => 'module:psspecialsviewstemplatesh',
      1 => 1613506025,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/productlist.tpl' => 1,
  ),
),false)) {
function content_60383db49a7999_50296516 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin C:\wamp64\www\cicorella/themes/cicorella/modules/ps_specials/views/templates/hook/ps_specials.tpl -->
<section class="featured-products clearfix mt-3">
  <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/productlist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('products'=>$_smarty_tpl->tpl_vars['products']->value), 0, false);
?>
</section>
<!-- end C:\wamp64\www\cicorella/themes/cicorella/modules/ps_specials/views/templates/hook/ps_specials.tpl --><?php }
}
