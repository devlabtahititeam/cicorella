<?php
/* Smarty version 3.1.34-dev-7, created on 2021-02-26 01:15:48
  from 'module:cicorellaviewstemplatesho' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_60383db4a09437_98789770',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '12edfc37a2d73802b3be40c85bf637894983a10f' => 
    array (
      0 => 'module:cicorellaviewstemplatesho',
      1 => 1613505806,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60383db4a09437_98789770 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin C:\wamp64\www\cicorella/modules/cicorella/views/templates/hook/cicorella.tpl -->


<div class="container">

    <h3 class="cicorellaModule_h3">Nos selections</h3>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="nouveautes-tab" data-toggle="tab" href="#nouveautes" role="tab" aria-controls="nouveautes" aria-selected="true">Nouveautés</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="populaires-tab" data-toggle="tab" href="#populaires" role="tab" aria-controls="populaires" aria-selected="false">Populaires</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="promotions-tab" data-toggle="tab" href="#promotions" role="tab" aria-controls="promotions" aria-selected="false">Promotions</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane  show active" id="nouveautes" role="tabpanel" aria-labelledby="home-tab">
            <?php echo $_smarty_tpl->tpl_vars['HOOK_NOUVEAUTES']->value;?>

        </div>
        <div class="tab-pane " id="populaires" role="tabpanel" aria-labelledby="profile-tab">
            <?php echo $_smarty_tpl->tpl_vars['HOOK_POPULAIRES']->value;?>

        </div>
        <div class="tab-pane " id="promotions" role="tabpanel" aria-labelledby="profile-tab">
            <?php echo $_smarty_tpl->tpl_vars['HOOK_PROMOTIONS']->value;?>

        </div>
    </div>

</div>
<!-- end C:\wamp64\www\cicorella/modules/cicorella/views/templates/hook/cicorella.tpl --><?php }
}
