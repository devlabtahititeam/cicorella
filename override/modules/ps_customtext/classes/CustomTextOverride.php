<?php

require_once _PS_MODULE_DIR_ . 'ps_customtext/classes/CustomText.php';

class CustomTextOverride extends CustomText
{
    public $image;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = [
        'table' => 'info',
        'primary' => 'id_info',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => [
            'id_info' => ['type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'],
            // Lang fields
            'text' => ['type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true],
            'image' => ['type' => self::TYPE_STRING, 'required' => true],
        ],
    ];
}
