<?php
/**
 * Override Class ProductCore
 */
class Productezaeaea extends ProductCore {

	public $banner_title;
	public $banner_subtitle;
	public $banner_text;

	public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null){
	 
			self::$definition['fields']['banner_title'] = [
	            'type' => self::TYPE_STRING,
	            'required' => false, 'size' => 255
	        ];
	        self::$definition['fields']['banner_subtitle'] = [
	            'type' => self::TYPE_STRING,
	            'lang' => true,
	            'required' => false, 'size' => 255
	        ];
	        self::$definition['fields']['banner_text'] = [
	            'type' => self::TYPE_HTML,
	            'lang' => true,
	            'required' => false,
	            'validate' => 'isCleanHtml'
	        ];

	        parent::__construct($id_product, $full, $id_lang, $id_shop, $context);
	}
}