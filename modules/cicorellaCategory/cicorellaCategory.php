<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class cicorellaCategory extends Module implements WidgetInterface
{
    public function __construct()
    {
        $this->name = 'cicorellaCategory';
        $this->version = '1.0.0';
        $this->author = 'Alexis - devlab';
        $this->need_instance = 0;

        $this->bootstrap = true;

        parent::__construct();

        $this->ps_versions_compliancy = [
            'min' => '1.7',
            'max' => _PS_VERSION_
        ];
        $this->displayName = $this->l('CicorellaCategory');
        $this->templateFile = 'module:cicorellaCategory/views/templates/hook/cicorellaCategory.tpl';
    }


    public function install()
    {
        return parent::install() && $this->registerHook('displayHome');
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }


    public function getWidgetVariables($hookName, array $configuration)
    {

        $lang = (int)Context::getContext()->language->id;
        $cats = Category::getCategories($lang);
//        var_dump($cats);
//        $this->smarty->assign('categories', $cats);





        return array(
            'HOOK_NOUVEAUTES' => Hook::exec('displayNouveautes'),
            'HOOK_POPULAIRES' => Hook::exec('displayPopulaires'),
            'categories' => $cats
        );
    }

    public function renderWidget($hookName, array $configuration)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('cicorellaCategory'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('cicorellaCategory'));
    }
}