<?php
class cicorelladisplayModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $this->setTemplate('module:cicorella/views/templates/front/display.tpl');
    }
}