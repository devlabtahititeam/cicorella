


<div class="container">

    <h3 class="cicorellaModule_h3">Nos selections</h3>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="nouveautes-tab" data-toggle="tab" href="#nouveautes" role="tab" aria-controls="nouveautes" aria-selected="true">Nouveautés</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="populaires-tab" data-toggle="tab" href="#populaires" role="tab" aria-controls="populaires" aria-selected="false">Populaires</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="promotions-tab" data-toggle="tab" href="#promotions" role="tab" aria-controls="promotions" aria-selected="false">Promotions</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane  show active" id="nouveautes" role="tabpanel" aria-labelledby="home-tab">
            {$HOOK_NOUVEAUTES nofilter}
        </div>
        <div class="tab-pane " id="populaires" role="tabpanel" aria-labelledby="profile-tab">
            {$HOOK_POPULAIRES nofilter}
        </div>
        <div class="tab-pane " id="promotions" role="tabpanel" aria-labelledby="profile-tab">
            {$HOOK_PROMOTIONS nofilter}
        </div>
    </div>

</div>
