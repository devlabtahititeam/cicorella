<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Cicorella extends Module implements WidgetInterface
{
    public function __construct()
    {
        $this->name = 'cicorella';
        $this->version = '1.0.0';
        $this->author = 'Alexis - devlab';
        $this->need_instance = 0;

        $this->bootstrap = true;

        parent::__construct();

        $this->ps_versions_compliancy = [
            'min' => '1.7',
            'max' => _PS_VERSION_
        ];
        $this->displayName = $this->l('Custom Cicorella');
        $this->templateFile = 'module:cicorella/views/templates/hook/cicorella.tpl';
    }


    public function install()
    {
        return parent::install() && $this->registerHook('displayHome') && $this->registerHook('displayNouveautes') && $this->registerHook('displayPopulaires') && $this->registerHook('displayPromotions');
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }


    public function getWidgetVariables($hookName, array $configuration)
    {

//        var_dump(Hook::exec('displayPromotions'));
//        var_dump(Hook::exec('displayNouveautes'));
        return array(
            'HOOK_NOUVEAUTES' => Hook::exec('displayNouveautes'),
            'HOOK_POPULAIRES' => Hook::exec('displayPopulaires'),
            'HOOK_PROMOTIONS' => Hook::exec('displayPromotions')
        );
    }

    public function renderWidget($hookName, array $configuration)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('cicorella'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('cicorella'));
    }
}