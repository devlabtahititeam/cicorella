{assign var=_counter value=0}
{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
        <ul class="top-menu" {if $depth == 0}id="top-menu"{/if} data-depth="{$depth}">
            {foreach from=$nodes item=node}
                <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">
                    {assign var=_counter value=$_counter+1}
                    {if $node.children|count}
                    {* Cannot use page identifier as we can have the same page several times *}
                    {assign var=_expand_id value=10|mt_rand:100000}
                    <a
                            class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}"

                            data-depth="{$depth}"
                            {if $node.open_in_new_window} target="_blank" {/if}
                            data-target="#top_sub_menu_{$_expand_id}"
                            data-toggle="collapse"
                            style="cursor: pointer;"
                    >

                            <span class="float-xs-right hidden-md-up">
                                <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse"
                                      class="navbar-toggler collapse-icons">
                                  <i class="material-icons add">&#xE313;</i>
                                  <i class="material-icons remove">&#xE316;</i>
                                </span>
                            </span>

                        {if {$node.page_identifier}=='category-2'}

                            {l s='Our Products' d='Shop.Menu.Actions'}
                        {elseif {$node.page_identifier}=='cms-category-1'}

                            {l s='Our jewelry' d='Shop.Menu.Actions'}
                        {else}
                            {$node.label}
                        {/if}
                    </a>
                    {else}
                        <a
                                class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}"
                                href="{$node.url}"
                                data-depth="{$depth}"
                                {if $node.open_in_new_window} target="_blank" {/if}
                        >

                            {if {$node.page_identifier}=='category-2'}

                                {l s='Our Products' d='Shop.Menu.Actions'}
                            {elseif {$node.page_identifier}=='cms-category-1'}

                                {l s='Our jewelry' d='Shop.Menu.Actions'}
                            {else}
                                {$node.label}
                            {/if}
                        </a>
                    {/if}
                    {if $node.children|count}
                        <div {if $depth === 0} class="popover sub-menu js-sub-menu collapse"{else} class="collapse"{/if}
                                id="top_sub_menu_{$_expand_id}">
                            {menu nodes=$node.children depth=$node.depth parent=$node}
                        </div>
                    {/if}
                </li>
            {/foreach}
        </ul>
    {/if}
{/function}

<div class="menu js-top-menu position-static hidden-sm-down" id="_desktop_top_menu">
    {menu nodes=$menu.children}
    <div class="clearfix"></div>
</div>
<script>

    // document.querySelector('a.mydropdown').addEventListener('click',function(e){
    //     console.log('mydropdown ____________________________________')
    //     var elementToToggle = this.getAttribute('data-target')
    //     console.log(elementToToggle)
    //     console.log(
    //         document.querySelector(elementToToggle)
    //     )
    // })



</script>


{*{assign var=_counter value=0}*}
{*{function name="menu" nodes=[] depth=0 parent=null}*}
{*    {if $nodes|count}*}
{*        HERE---{$depth}*}

{*        tttttttttttttttttttttt*}
{*      <ul class="top-menu" {if $depth == 0}id="top-menu"{/if} data-depth="{$depth}">*}

{*         <li><p>TTTTTTT---{$depth}</p></li>*}

{*        {foreach from=$nodes item=node}*}

{*            <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">*}
{*            {assign var=_counter value=$_counter+1}*}
{*              <a*}
{*                class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}"*}
{*                href="{$node.url}" data-depth="{$depth}"*}
{*                {if $node.open_in_new_window} target="_blank" {/if}*}
{*              >*}
{*                {if $node.children|count}*}
{*                  *}{* Cannot use page identifier as we can have the same page several times *}
{*                  {assign var=_expand_id value=10|mt_rand:100000}*}
{*                  <span class="float-xs-right hidden-md-up">*}
{*                    <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse" class="navbar-toggler collapse-icons">*}
{*                      <i class="material-icons add">&#xE313;</i>*}
{*                      <i class="material-icons remove">&#xE316;</i>*}
{*                    </span>*}
{*                  </span>*}
{*                {/if}*}

{*                  {if {$node.page_identifier}=='category-2'}*}

{*                      {l s='Our Products' d='Shop.Menu.Actions'}*}
{*                  {elseif {$node.page_identifier}=='cms-category-1'}*}

{*                      {l s='Our jewelry' d='Shop.Menu.Actions'}*}
{*                  {else}*}
{*                      {$node.label}*}
{*                  {/if}*}

{*              </a>*}
{*              {if $node.children|count}*}
{*              <div {if $depth === 0} class="popover sub-menu js-sub-menu collapse"{else} class="collapse"{/if} id="top_sub_menu_{$_expand_id}">*}
{*                {menu nodes=$node.children depth=$node.depth parent=$node}*}
{*              </div>*}
{*              {/if}*}
{*            </li>*}
{*        {/foreach}*}
{*      </ul>*}
{*    {/if}*}
{*{/function}*}
{*<div class="menu js-top-menu position-static" id="_desktop_top_menu">*}
{*    {menu nodes=$menu.children}*}
{*    <div class="clearfix"></div>*}
{*</div>*}
